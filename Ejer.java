import java.util.Arrays;

public class Ejer {
public static void main(String[] args) {
	
	int moitonumeros[] = new int[10];
	rellenar(moitonumeros);
	modificacion(moitonumeros);
	numElem0yDistin0(moitonumeros);
}
	public static void rellenar(int numeros[]) {
		for (int i = 0; i < numeros.length; i++) {
			numeros[i]=i;
		}
		System.out.println(Arrays.toString(numeros));
	}
	public static void modificacion(int numeros[]) {
		for (int i = 0; i < numeros.length; i++) {
			if (i%2==0) {
				numeros[i]=0;
			}
		}
	}
	
	public static void numElem0yDistin0(int numeros[]) {
		int totalno0 = 0;
		for (int i = 0; i < numeros.length; i++) {
			if (numeros[i]!=0) {
				totalno0++;
			}
		}
		System.out.println("Numero de 0 en el array: "+totalno0+" Numero de distinto a 0 en el array: "+(numeros.length-totalno0));
	}
}
